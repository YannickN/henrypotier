import React, { Component } from 'react'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import { createStyles } from '@material-ui/core'
import Truncate from 'react-truncate'
import AddShoppingCart from '@material-ui/icons/AddShoppingCart'
import RemoveShoppingCart from '@material-ui/icons/RemoveShoppingCart'
import ShoppingCart from '@material-ui/icons/ShoppingCart'
import { Theme } from '@material-ui/core/styles/createMuiTheme'
import ShopContext from '../context/shop-context'

const styles = ({ spacing }: Theme) => createStyles({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    cardMedia: {
        paddingTop: '56.25%' // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    leftIcon: {
        marginRight: spacing.unit
    },
})

class ListBook extends Component<any, any> {
    static contextType = ShopContext
    render() {
        const { listBooks, classes } = this.props
        return (
            <Grid container spacing={40}>
                {listBooks.map((book: any, index: number) => (
                    <Grid item key={index} sm={6} md={4} lg={3}>
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image={book.cover}
                                title={book.title}
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {book.title}
                                </Typography>
                                <Typography>
                                    <Truncate lines={3} ellipsis={<span>... </span>}>
                                        {book.synopsis[0]}
                                    </Truncate>
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button size="small" color="primary">
                                    Voir le livre
                                </Button>
                                <Button size="small" color="primary" onClick={this.context.addBookToCart.bind(this, book)}>
                                    <ShoppingCart className={classes.leftIcon} />
                                    Ajouter
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        )
    }
}

export default withStyles(styles)(ListBook)

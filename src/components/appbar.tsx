import React, { SFC } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { createStyles } from '@material-ui/core'
import { Theme } from '@material-ui/core/styles/createMuiTheme'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import BookIcon from '@material-ui/icons/Book'
import Navigation from '../components/navigation'

const styles = ({ spacing }: Theme) => createStyles({
    appBar: {
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    icon: {
        marginRight: spacing.unit * 2
    },
    toolbar: {
        width: '30%'
    },
    divHidden: {
        width: '30%',
        marginRight: 24,
        marginLeft: 24,
        padding: '2px 4px'
    }
})

type Props = {
    classes: any
    cartBook: any
    children?: any
}

const Appbar: SFC<Props> = ({ classes, cartBook, children }) => (
    <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
            <BookIcon className={classes.icon} />
            <Typography variant="h6" color="inherit" noWrap>
                WelcomeReader
            </Typography>
        </Toolbar>
        <Navigation
            cartItemNumber={cartBook.reduce((count: any, curItem: any) => {
                return count + curItem.quantity;
            }, 0)}
        />
        {children ? children : <div className={classes.divHidden}></div>}
    </AppBar>
)

export default withStyles(styles)(Appbar)

import React from 'react'
import { NavLink } from 'react-router-dom'
import '../stylesheets/navigation.css'
import Button from '@material-ui/core/Button'

const Navigation = (props: any) => (
    <div className="main-navigation">
        <nav>
            <ul>
                <li>
                    <NavLink to="/">
                        <Button size="small" variant="contained" color="primary" className="buttonNav">
                            Books
                        </Button>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/cart">
                        <Button size="small" variant="contained" color="primary" className="buttonNav">
                            Cart ({props.cartItemNumber})
                        </Button>
                    </NavLink>
                </li>
            </ul>
        </nav>
    </div>
)

export default Navigation

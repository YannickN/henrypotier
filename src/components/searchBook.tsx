import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import ShopContext from '../context/shop-context'
import { createStyles } from '@material-ui/core'

const styles = createStyles({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: '30%',
        marginRight: 24,
        marginLeft: 24
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4,
    },
})

class SearchBook extends Component<any, any> {
    static contextType = ShopContext

    render() {
        const { classes, searchBook } = this.props
        return (
            <Paper className={classes.root} elevation={1}>
                <InputBase className={classes.input} placeholder="Search..." onChange={this.context.filterList.bind(this, searchBook)} />
                <Divider className={classes.divider} />
                <IconButton className={classes.iconButton} aria-label="Search">
                    <SearchIcon />
                </IconButton>
            </Paper>
        )
    }
}

export default withStyles(styles)(SearchBook)

import React, { Component } from 'react'
import axios from 'axios'

import ShopContext from './shop-context'

// const URL_API = process.env.REACT_APP_URL_BOOKS
const URL_API = 'http://henri-potier.xebia.fr/books'

class GlobalState extends Component {
    state = {
        listBooks: Array<any>(),
        listBooksFiltered: Array<any>(),
        cartBook: Array<any>()
    }

    componentDidMount() {
        axios.get(URL_API)
            .then(({ data }) => {
                this.setState({ listBooks: data, listBooksFiltered: data })
            })
            .catch(() => {
                this.setState({ listBooks: Array<any>() })
            })
    }

    addBookToCart = (book: any) => {
        console.log('Adding book', book)
        const updatedCart: any = [...this.state.cartBook]
        const updatedItemIndex = updatedCart.findIndex(
            (item: any) => item.isbn === book.isbn
        )

        if (updatedItemIndex < 0) {
            updatedCart.push({ ...book, quantity: 1 })
        } else {
            const updatedItem = {
                ...updatedCart[updatedItemIndex]
            }
            updatedItem.quantity++
            updatedCart[updatedItemIndex] = updatedItem
        }
        this.setState({ cartBook: updatedCart })
    }

    removeBookFromCart = (bookId: any) => {
        console.log('Removing book with isbn: ' + bookId)
        const updatedCart: any = [...this.state.cartBook]
        const updatedItemIndex = updatedCart.findIndex(
            (item: any) => item.isbn === bookId
        )

        const updatedItem = {
            ...updatedCart[updatedItemIndex]
        }
        updatedItem.quantity--
        if (updatedItem.quantity <= 0) {
            updatedCart.splice(updatedItemIndex, 1)
        } else {
            updatedCart[updatedItemIndex] = updatedItem
        }
        this.setState({ cartBook: updatedCart })
    }

    filterList = (searchBook: any, event: any) => {
        let updatedList = searchBook
        updatedList = updatedList.filter((item: any) => item.title.toLowerCase().search(event.target.value.toLowerCase()) !== -1)
        this.setState({ listBooksFiltered: updatedList })
    }

    render() {
        return (
            <ShopContext.Provider
                value={{
                    listBooks: this.state.listBooks,
                    listBooksFiltered: this.state.listBooksFiltered,
                    cartBook: this.state.cartBook,
                    addBookToCart: this.addBookToCart,
                    removeBookFromCart: this.removeBookFromCart,
                    filterList: this.filterList
                }}
            >
                {this.props.children}
            </ShopContext.Provider>
        )
    }
}

export default GlobalState

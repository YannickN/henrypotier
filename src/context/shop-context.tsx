import React from 'react'

export default React.createContext({
    listBooks: Array<any>(),
    listBooksFiltered: Array<any>(),
    cartBook: Array<any>(),
    addBookToCart: (book: any) => {},
    removeBookFromCart: (bookId: any) => {},
    filterList: (searchBook: any, event: any) => {}
})

import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import GlobalState from './context/globalState'
import BookPage from './pages/book'
import CartPage from './pages/cart'
import logo from './logo.svg'
import './App.css'

class App extends Component {
  render() {
    return (
        <GlobalState>
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={BookPage} exact />
                    <Route path="/cart" component={CartPage} exact />
                </Switch>
            </BrowserRouter>
        </GlobalState>
    )
  }
}

export default App

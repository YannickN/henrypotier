import React, { Component, Fragment } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import ShopContext from '../context/shop-context'
import Navigation from '../components/navigation'
import Appbar from '../components/appbar'
import ReceiptIcon from '@material-ui/icons/Receipt'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'
import LocalOfferIcon from '@material-ui/icons/LocalOffer'
import Divider from '@material-ui/core/Divider'
import Tooltip from '@material-ui/core/Tooltip'
import axios from 'axios'
import '../stylesheets/cart.css'


const URL_API = 'http://henri-potier.xebia.fr/books/'

class CartPage extends Component {
    static contextType = ShopContext
    state = {
        sum: 0,
        commercialOffers: Array<any>(),
        minus: 0
    }

    componentDidMount() {
        let isbns = ''
        let sum = 0
        if (this.context.cartBook.length > 0) {
            this.context.cartBook.map((item: any) => {
                isbns += `${item.isbn},`
                sum += item.price
            })
            isbns = isbns.replace(/.$/, '')
            axios.get(`${URL_API + isbns}/commercialOffers`)
                .then(({ data }) => {
                    console.log(data)
                    let minus = 0
                    data.offers.map((offer: any) => {
                        if (offer.type === "minus") {
                            minus = offer.value
                        }
                    })
                    console.log(minus)
                    this.setState({ commercialOffers: data.offers, minus })
                })
                .catch(() => {
                    console.log('Something wrong')
                })
        }
        this.setState({ sum })
    }

    handleClickOffers = (type: any) => {
        const { sum, commercialOffers } = this.state
        // use switch
        let newSum = 0
        commercialOffers.map((offer: any) => {
            if (offer.type === type) {
                newSum = sum - offer.value
            }
        })
        this.setState({ sum: newSum  })
    }

    render() {
        const { sum } = this.state
        return (
            <Fragment>
                <CssBaseline />
                <Appbar
                    cartBook={this.context.cartBook}
                />
                <main className="cart">
                    {this.context.cartBook.length <= 0 && <p>No Item in the Cart!</p>}
                    <ul>
                        {this.context.cartBook.map((cartItem: any) => (
                            <li key={cartItem.isbn}>
                                <Card className="cardflex">
                                    <div className="details">
                                        <CardContent className="content">
                                            <Typography component="h5" variant="h5">
                                                {cartItem.title}
                                            </Typography>
                                            <Typography variant="subtitle1" color="textSecondary">
                                                ${cartItem.price} ({cartItem.quantity})
                                            </Typography>
                                        </CardContent>
                                        <div className="controls">
                                            <Button size="small" color="primary" onClick={this.context.removeBookFromCart.bind(this, cartItem.isbn)}>
                                                <DeleteIcon className="leftIcon" />
                                                Supprimer
                                            </Button>
                                        </div>
                                    </div>
                                    <CardMedia
                                        className="cover"
                                        image={cartItem.cover}
                                        title={cartItem.title}
                                    />
                                </Card>
                            </li>
                        ))}
                    </ul>
                </main>
                <div className='container-card'>
                    <Card className='cardflex'>
                        <CardContent>
                            <Typography variant="h5" component="h2">
                                <ReceiptIcon className='receipt' />
                                ${sum}
                            </Typography>
                        </CardContent>
                        <Divider className="divider" />
                        <Tooltip title={`-$${this.state.minus}`} placement="left">
                            <Button size="small" color="primary" onClick={this.handleClickOffers.bind(this, "minus")}>
                                <LocalOfferIcon className="leftIcon" />
                                Special Offer
                            </Button>
                        </Tooltip>
                    </Card>
                </div>
            </Fragment>
        )
    }
}

export default CartPage

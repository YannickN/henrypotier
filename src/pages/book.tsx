import React, { Component, Fragment } from 'react'
import classNames from 'classnames'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import { createStyles } from '@material-ui/core'
import { Theme } from '@material-ui/core/styles/createMuiTheme'
import SearchBook from '../components/searchBook'
import ListBook from '../components/listBook'
import ShopContext from '../context/shop-context'
import Appbar from '../components/appbar'
import '../stylesheets/book.css'

const styles = ({ palette, spacing, breakpoints }: Theme) => createStyles({
    heroUnit: {
        backgroundColor: palette.background.paper
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${spacing.unit * 8}px 0 ${spacing.unit * 6}px`
    },
    heroButtons: {
        marginTop: spacing.unit * 4
    },
    layout: {
        width: 'auto',
        marginLeft: spacing.unit * 3,
        marginRight: spacing.unit * 3,
        [breakpoints.up(1100 + spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto'
        }
    },
    cardGrid: {
        padding: `${spacing.unit * 8}px 0`
    },
    footer: {
        backgroundColor: palette.background.paper,
        padding: spacing.unit * 6
    }
})

class Book extends Component<any, any> {

    render() {
        const { classes } = this.props
        return (
            <ShopContext.Consumer>
                {context => (
                    <Fragment>
                        <CssBaseline />
                        <Appbar
                            children={<SearchBook searchBook={context.listBooks} />}
                            cartBook={context.cartBook}
                        />
                        <main>
                            <div className={classes.heroUnit}>
                                <div className={classes.heroContent}>
                                    <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                                        WelcomeReader
                            </Typography>
                                    <Typography variant="h6" align="center" color="textSecondary" paragraph>
                                        Once upon a time...
                            </Typography>
                                </div>
                            </div>
                            <div className={classNames(classes.layout, classes.cardGrid)}>
                                <ListBook listBooks={context.listBooksFiltered} />
                            </div>
                        </main>
                        <footer className={classes.footer}>
                            <Typography variant="h6" align="center" gutterBottom>
                                Footer
                </Typography>
                            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                                Something here to give the footer a purpose!
                </Typography>
                        </footer>
                    </Fragment>
                )}
            </ShopContext.Consumer>
        )
    }
}

export default withStyles(styles)(Book)
